FROM node:12.16.2

WORKDIR /opt/seagull
COPY package*.json ./ 
RUN npm install
COPY . .
EXPOSE 4444
CMD npm start