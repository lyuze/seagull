import React from 'react';
import styles from './index.module.scss';
import Header from '@/components/Header';

const pic1 = require('./images/1.png');
const pic2 = require('./images/2.jpg');
const pic3 = require('./images/3.jpg');
const pic4 = require('./images/4.jpg');
const pic5 = require('./images/5.jpg');

export default function Collection1() {
    return (
        <div>
            <Header />
            <div className={styles.show}>
                <img src={pic1} className={styles.pic} />
                <img src={pic2} className={styles.pic} />
                <img src={pic3} className={styles.pic} />
                <img src={pic4} className={styles.pic} />
                <img src={pic5} className={styles.pic} />
            </div>

        </div>
    );
}