import React from 'react';
import styles from './index.module.scss';
import Header from '@/components/Header';

const pic1 = require('./images/missyou.jpg');

export default function MissYou() {
    return (
        <div>
            <Header />
            <div className={styles.show}>
                <img src={pic1} className={styles.pic} />
            </div>

        </div>
    );
}